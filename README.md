Thalie 🎭 A LaTeX package providing tools to typeset drama plays
===============================================================

It defines commands to introduce characters' lines, to render stage direction,
to divide a play into acts and scenes, to automatically build the dramatis
personae, etc.

- Version 0.13a (2022-12-11)
- Usage and installation instruction are available in the [documentation](http://mirrors.ctan.org/macros/latex/contrib/thalie/thalie.pdf).
- Examples
  - [French play, in verse](https://spalax.frama.io/thalie/examples/cyrano.pdf) ([source](https://framagit.org/spalax/thalie/-/blob/main/examples/cyrano.tex))
  - [French play, in prose](https://spalax.frama.io/thalie/examples/domjuan.pdf) ([source](https://framagit.org/spalax/thalie/-/blob/main/examples/domjuan.tex))
  - [English play, in prose](https://spalax.frama.io/thalie/examples/hamlet.pdf) ([source](https://framagit.org/spalax/thalie/-/blob/main/examples/hamlet.tex))
  - [English play, in verse](https://spalax.frama.io/thalie/examples/henry.pdf) ([source](https://framagit.org/spalax/thalie/-/blob/main/examples/henry.tex))

License
-------

*Copyright 2010-2022 Louis Paternault*

This work may be distributed and/or modified under the conditions of the LaTeX
Project Public License, either version 1.3 of this license or (at your option)
any later version.

The latest version of this license is in http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX version
2005/12/01 or later.
