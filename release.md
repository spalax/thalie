How to release
==============

0. Check that everything compile:

        make all examples

0. Update version number and release date, at least in: CHANGELOG.md README thalie.dtx thalie.ins

        git grep CURRENT_VERSION

0. Change copyright date, at least in: README thalie.ins thalie.dtx

        git grep CURRENT_COPYRIGHT_YEAR

0. Update changelog (in file CHANGELOG.md and in thalie.dtx)

        $EDITOR CHANGELOG.md

0. Update date and changelog in the .dtx file (look for "vX.XX" strings).

0. Add up-to-date URLs to examples in the README.

0. Make sure README is correctly rendered by CTAN

        https://www.ctan.org/markdown

0. Mark release in git

        git commit -m "Version VERSION"
        git tag vVERSION

0. Build CTAN package

    make build

0. Upload to CTAN: go to http://ctan.org/pkg/thalie and click on `Upload` (to pre-fill some fields).
