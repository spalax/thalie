# Non backward compatible version

* Make `xspace` package option default to `false`.
* Review the default value for `characterstyle` and `{play,act,scene}style`
  package options.
* Remove commands `\playname`, `\actname`, `\scenename`, `\interludename`, `\curtainname`, `\pausename`.
