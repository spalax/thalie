.PHONY: all
all: thalie.pdf thalie.sty examples

.PHONY: build
build: thalie.pdf thalie.tex thalie.sty CHANGELOG.md README.md LICENSE.txt CHANGELOG.md dicts/*.trsl
	ctanify thalie.tex thalie.sty thalie.pdf README.md LICENSE.txt CHANGELOG.md dicts/*.trsl=tex/latex/thalie/

.PHONY: public
public: examples # thalie.pdf
	mkdir -p public/examples
	cp examples/*pdf public/examples

.PHONY: examples
examples: thalie.sty
	cd examples && make examples

thalie.pdf: thalie.tex thalie.sty examples/cyrano.tex
	lualatex thalie.tex
	lualatex thalie.tex

.PHONY: clean
allclean: clean
	rm -f \
		thalie.pdf \

.PHONY: allclean
clean:
	rm -f thalie.aux \
		thalie.glo \
		thalie.gls \
		thalie.idx \
		thalie.ilg \
		thalie.ind \
		thalie.log \
		thalie.out \
		thalie.toc
